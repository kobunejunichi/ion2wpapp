import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { Http, Request, Response, RequestOptionsArgs, ConnectionBackend, RequestOptions } from "@angular/http";
@Injectable()
export class CustomHttp extends Http {
  constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
    super(backend, defaultOptions);
  }
  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    if (url instanceof Request) {
      // HTTPヘッダにカスタムヘッダを追加
      url.headers.append("app-version", "3.0.1");
      url.headers.append("platform", "ios");
    }
    console.log("##############");
    console.log(url);

    // console.dump(options);
    return super.request(url, options);        
  }

}