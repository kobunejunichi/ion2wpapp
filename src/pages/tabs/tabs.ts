import { Component } from '@angular/core';

import { FeedPage } from '../feed/feed';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { MembersPage } from '../members/members';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = FeedPage;
  tab2Root: any = AboutPage;
  tab3Root: any = ContactPage;
  tab4Root: any = MembersPage;

  constructor() {

  }
}
