import { Component } from '@angular/core';
import { HomePage } from "../home/home";
import { ModalController } from 'ionic-angular';
import { PreviewModal } from '../preview/preview';
import { GroupApi, ArticleApi } from '../../app/api/services';
import { Group, Article } from '../../app/api/models';

@Component({
  selector: 'page-home',
  templateUrl: 'feed.html'
})
export class FeedPage {

  articles: [Article]

  constructor(private modalCtrl: ModalController, private groupApi: GroupApi, private articleApi: ArticleApi) {
    // グループ一覧を取得
    groupApi.find().subscribe(
      (groups: [Group]) => {
        console.log("GROUPS", groups[0]);
        // とりあえずグループ一覧の１件目を対象とする
        let group = groups[2];
        let groupId = group.id;
        let pageId = group.pages[0].pageId;
        let query = {
          groupId: groupId,
          pageId: pageId
        }
        console.log("QUERY", query);
        // 記事一覧を取得する
        articleApi.find(query).subscribe(
          (articles: [Article]) => {
            console.log("ARTICLES", articles);
            this.articles = articles;
          },
          (error) => {
            console.log("ERROR", error);
          }
        );


      },
      (error) => {
        console.log("ERROR", error);
      }
    );
  }

  openPreview(contents) {
    console.log("%%%", contents);
    let previewModal = this.modalCtrl.create(PreviewModal, {contents: contents});
    previewModal.present();
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

}
