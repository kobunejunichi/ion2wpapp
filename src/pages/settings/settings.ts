import { Component } from '@angular/core';
import { NotificationsPage } from '../notifications/notifications';

@Component({
  templateUrl: "settings.html"
})
export class SettingsPage {
  notificationsPage = NotificationsPage;
  constructor() {}
}