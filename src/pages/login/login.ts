import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { AppUserApi } from "../../app/api/services";
import { AppUser } from "../../app/api/models";
import { TabsPage } from "../tabs/tabs";

@Component({
  templateUrl: "login.html",

})
export class LoginPage {
  credentials = {
    email: "kobune.junichi+100@gmail.com",
    password: "aaaaaaaa",
    rememberMe: true
  };
  constructor(public navCtrl: NavController, private userApi: AppUserApi) {
    // 認証チェック
    userApi.getAuthentication().subscribe(
      (response) => {
        console.log("RESPONSE", response);
        if (response.result) {
          // 認証済みだった場合はホーム画面へ遷移
          this.navCtrl.setRoot(TabsPage);
        }
      },
      (error) => {
        console.log("ERROR", error);
      }
    );
  }

  login() {
    console.log(this.credentials);
    // ログイン処理
    this.userApi.login(this.credentials).subscribe(this.loginSuccess, this.loginError);
  }
  loginSuccess = (result) => {
    console.log("login success", result);
    // ログインが成功したらホーム画面へ遷移
    this.navCtrl.setRoot(TabsPage);
  }
  loginError = (e) => {
    console.log("login error", e);
  }
}