import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

@Component({
  templateUrl: "preview.html"

})
export class PreviewModal {
  contents = [];
  constructor(private viewCtrl: ViewController, private navParams: NavParams) {
    let contents = navParams.get("contents");
    console.log("###", navParams);
    contents.forEach((content) => {
      if (content.type === "image") {
        content.imageOriginals.forEach((origin) => {
          this.contents.push(origin);
        });
      }
    });

  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}