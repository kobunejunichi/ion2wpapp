import { Component } from '@angular/core';
import { MemberSettingPage } from '../member-setting/member-setting';

@Component({
  templateUrl: "members.html"
})
export class MembersPage {
  memberSettingPage = MemberSettingPage;
  constructor() {}
}