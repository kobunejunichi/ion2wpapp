import { NgModule, ErrorHandler } from '@angular/core';
import { Http, XHRBackend, RequestOptions } from "@angular/http";
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { SDKBrowserModule } from './api/index';

import { CustomHttp } from '../services/CustomHttp';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { FeedPage } from '../pages/feed/feed';
import { ContactPage } from '../pages/contact/contact';
import { AboutPage } from '../pages/about/about';
import { MembersPage } from '../pages/members/members';
import { MemberSettingPage } from '../pages/member-setting/member-setting';
import { TabsPage } from '../pages/tabs/tabs';
import { SettingsPage } from '../pages/settings/settings';
import { NotificationsPage } from '../pages/notifications/notifications';
import { PreviewModal } from '../pages/preview/preview';
import { LoginPage } from '../pages/login/login';

// LoopBackの設定
let LoopBackSDKModule = SDKBrowserModule.forRoot();
// LoopBackで使用するHTTPをカスタムHTTPに変更する
LoopBackSDKModule.providers.push(
  {
    provide: Http, 
    useFactory: (backend: XHRBackend, defaultOptions: RequestOptions) => new CustomHttp(backend, defaultOptions),
    deps: [XHRBackend, RequestOptions]
  }
);

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    FeedPage,
    ContactPage,
    AboutPage,
    MembersPage,
    MemberSettingPage,
    TabsPage,
    SettingsPage,
    NotificationsPage,
    PreviewModal,
    LoginPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    // SDKBrowserModule.forRoot()
    LoopBackSDKModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    FeedPage,
    ContactPage,
    AboutPage,
    MembersPage,
    MemberSettingPage,
    TabsPage,
    SettingsPage,
    NotificationsPage,
    PreviewModal,
    LoginPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
