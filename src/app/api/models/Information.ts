/* tslint:disable */

declare var Object: any;
export interface InformationInterface {
  name?: any;
  message?: any;
  informationDate?: Date;
  statusValid?: string;
  language?: string;
  id?: any;
  createdAt?: Date;
  createdBy?: string;
  modifiedAt?: Date;
  modifiedBy?: string;
}

export class Information implements InformationInterface {
  name: any;
  message: any;
  informationDate: Date;
  statusValid: string;
  language: string;
  id: any;
  createdAt: Date;
  createdBy: string;
  modifiedAt: Date;
  modifiedBy: string;
  constructor(data?: InformationInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Information`.
   */
  public static getModelName() {
    return "Information";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Information for dynamic purposes.
  **/
  public static factory(data: InformationInterface): Information{
    return new Information(data);
  }  
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Information',
      plural: 'Information',
      properties: {
        name: {
          name: 'name',
          type: 'any'
        },
        message: {
          name: 'message',
          type: 'any'
        },
        informationDate: {
          name: 'informationDate',
          type: 'Date'
        },
        statusValid: {
          name: 'statusValid',
          type: 'string'
        },
        language: {
          name: 'language',
          type: 'string'
        },
        id: {
          name: 'id',
          type: 'any'
        },
        createdAt: {
          name: 'createdAt',
          type: 'Date',
          default: new Date(0)
        },
        createdBy: {
          name: 'createdBy',
          type: 'string'
        },
        modifiedAt: {
          name: 'modifiedAt',
          type: 'Date',
          default: new Date(0)
        },
        modifiedBy: {
          name: 'modifiedBy',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
