/* tslint:disable */

declare var Object: any;
export interface ActivityInterface {
  version?: string;
  groupId?: string;
  userId?: string;
  activityType?: string;
  users?: any;
  messageOption?: any;
  targetInfo?: any;
  activityDate?: Date;
  statusDeleted?: string;
  id?: any;
  createdAt?: Date;
  createdBy?: string;
  modifiedAt?: Date;
  modifiedBy?: string;
}

export class Activity implements ActivityInterface {
  version: string;
  groupId: string;
  userId: string;
  activityType: string;
  users: any;
  messageOption: any;
  targetInfo: any;
  activityDate: Date;
  statusDeleted: string;
  id: any;
  createdAt: Date;
  createdBy: string;
  modifiedAt: Date;
  modifiedBy: string;
  constructor(data?: ActivityInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Activity`.
   */
  public static getModelName() {
    return "Activity";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Activity for dynamic purposes.
  **/
  public static factory(data: ActivityInterface): Activity{
    return new Activity(data);
  }  
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Activity',
      plural: 'Activities',
      properties: {
        version: {
          name: 'version',
          type: 'string'
        },
        groupId: {
          name: 'groupId',
          type: 'string'
        },
        userId: {
          name: 'userId',
          type: 'string'
        },
        activityType: {
          name: 'activityType',
          type: 'string'
        },
        users: {
          name: 'users',
          type: 'any'
        },
        messageOption: {
          name: 'messageOption',
          type: 'any'
        },
        targetInfo: {
          name: 'targetInfo',
          type: 'any'
        },
        activityDate: {
          name: 'activityDate',
          type: 'Date'
        },
        statusDeleted: {
          name: 'statusDeleted',
          type: 'string',
          default: '0'
        },
        id: {
          name: 'id',
          type: 'any'
        },
        createdAt: {
          name: 'createdAt',
          type: 'Date',
          default: new Date(0)
        },
        createdBy: {
          name: 'createdBy',
          type: 'string'
        },
        modifiedAt: {
          name: 'modifiedAt',
          type: 'Date',
          default: new Date(0)
        },
        modifiedBy: {
          name: 'modifiedBy',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
