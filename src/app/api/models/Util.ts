/* tslint:disable */

declare var Object: any;
export interface UtilInterface {
  id?: string;
}

export class Util implements UtilInterface {
  id: string;
  constructor(data?: UtilInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Util`.
   */
  public static getModelName() {
    return "Util";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Util for dynamic purposes.
  **/
  public static factory(data: UtilInterface): Util{
    return new Util(data);
  }  
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Util',
      plural: 'Utils',
      properties: {
        id: {
          name: 'id',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
