/* tslint:disable */

declare var Object: any;
export interface AppVersionInterface {
  os?: string;
  version?: string;
  id?: any;
  createdAt?: Date;
  createdBy?: string;
  modifiedAt?: Date;
  modifiedBy?: string;
}

export class AppVersion implements AppVersionInterface {
  os: string;
  version: string;
  id: any;
  createdAt: Date;
  createdBy: string;
  modifiedAt: Date;
  modifiedBy: string;
  constructor(data?: AppVersionInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `AppVersion`.
   */
  public static getModelName() {
    return "AppVersion";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of AppVersion for dynamic purposes.
  **/
  public static factory(data: AppVersionInterface): AppVersion{
    return new AppVersion(data);
  }  
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'AppVersion',
      plural: 'AppVersions',
      properties: {
        os: {
          name: 'os',
          type: 'string'
        },
        version: {
          name: 'version',
          type: 'string'
        },
        id: {
          name: 'id',
          type: 'any'
        },
        createdAt: {
          name: 'createdAt',
          type: 'Date',
          default: new Date(0)
        },
        createdBy: {
          name: 'createdBy',
          type: 'string'
        },
        modifiedAt: {
          name: 'modifiedAt',
          type: 'Date',
          default: new Date(0)
        },
        modifiedBy: {
          name: 'modifiedBy',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
