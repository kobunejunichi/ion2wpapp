/* tslint:disable */

declare var Object: any;
export interface AppUserInterface {
  name?: string;
  carrier?: string;
  devices?: any;
  memberCode?: string;
  imageLargeUrl?: string;
  imageUrl?: string;
  email?: string;
  emailChanged?: string;
  emailVerificationToken?: string;
  emailVerificationExpireDate?: Date;
  emailSendRevokeCount?: number;
  emailVerified?: string;
  password?: string;
  tmpPasswordExpireDate?: Date;
  notifications?: any;
  infoNotifications?: any;
  groups?: any;
  via?: string;
  joinDate?: Date;
  resignDate?: Date;
  resignReason?: any;
  dateLastLogined?: Date;
  revokeCount?: number;
  statusValid?: string;
  statusDeleted?: string;
  loginCount?: number;
  announceConfirmDate?: any;
  announceBadgeConfirmDate?: any;
  realm?: string;
  username?: string;
  challenges?: any;
  verificationToken?: string;
  status?: string;
  created?: Date;
  lastUpdated?: Date;
  id?: any;
  createdAt?: Date;
  createdBy?: string;
  modifiedAt?: Date;
  modifiedBy?: string;
  accessTokens?: any[];
}

export class AppUser implements AppUserInterface {
  name: string;
  carrier: string;
  devices: any;
  memberCode: string;
  imageLargeUrl: string;
  imageUrl: string;
  email: string;
  emailChanged: string;
  emailVerificationToken: string;
  emailVerificationExpireDate: Date;
  emailSendRevokeCount: number;
  emailVerified: string;
  password: string;
  tmpPasswordExpireDate: Date;
  notifications: any;
  infoNotifications: any;
  groups: any;
  via: string;
  joinDate: Date;
  resignDate: Date;
  resignReason: any;
  dateLastLogined: Date;
  revokeCount: number;
  statusValid: string;
  statusDeleted: string;
  loginCount: number;
  announceConfirmDate: any;
  announceBadgeConfirmDate: any;
  realm: string;
  username: string;
  challenges: any;
  verificationToken: string;
  status: string;
  created: Date;
  lastUpdated: Date;
  id: any;
  createdAt: Date;
  createdBy: string;
  modifiedAt: Date;
  modifiedBy: string;
  accessTokens: any[];
  constructor(data?: AppUserInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `AppUser`.
   */
  public static getModelName() {
    return "AppUser";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of AppUser for dynamic purposes.
  **/
  public static factory(data: AppUserInterface): AppUser{
    return new AppUser(data);
  }  
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'AppUser',
      plural: 'AppUsers',
      properties: {
        name: {
          name: 'name',
          type: 'string'
        },
        carrier: {
          name: 'carrier',
          type: 'string'
        },
        devices: {
          name: 'devices',
          type: 'any',
          default: <any>null
        },
        memberCode: {
          name: 'memberCode',
          type: 'string'
        },
        imageLargeUrl: {
          name: 'imageLargeUrl',
          type: 'string'
        },
        imageUrl: {
          name: 'imageUrl',
          type: 'string'
        },
        email: {
          name: 'email',
          type: 'string'
        },
        emailChanged: {
          name: 'emailChanged',
          type: 'string'
        },
        emailVerificationToken: {
          name: 'emailVerificationToken',
          type: 'string'
        },
        emailVerificationExpireDate: {
          name: 'emailVerificationExpireDate',
          type: 'Date'
        },
        emailSendRevokeCount: {
          name: 'emailSendRevokeCount',
          type: 'number',
          default: 0
        },
        emailVerified: {
          name: 'emailVerified',
          type: 'string',
          default: '1'
        },
        password: {
          name: 'password',
          type: 'string'
        },
        tmpPasswordExpireDate: {
          name: 'tmpPasswordExpireDate',
          type: 'Date'
        },
        notifications: {
          name: 'notifications',
          type: 'any'
        },
        infoNotifications: {
          name: 'infoNotifications',
          type: 'any'
        },
        groups: {
          name: 'groups',
          type: 'any',
          default: <any>null
        },
        via: {
          name: 'via',
          type: 'string'
        },
        joinDate: {
          name: 'joinDate',
          type: 'Date'
        },
        resignDate: {
          name: 'resignDate',
          type: 'Date'
        },
        resignReason: {
          name: 'resignReason',
          type: 'any'
        },
        dateLastLogined: {
          name: 'dateLastLogined',
          type: 'Date'
        },
        revokeCount: {
          name: 'revokeCount',
          type: 'number',
          default: 0
        },
        statusValid: {
          name: 'statusValid',
          type: 'string',
          default: '1'
        },
        statusDeleted: {
          name: 'statusDeleted',
          type: 'string',
          default: '0'
        },
        loginCount: {
          name: 'loginCount',
          type: 'number',
          default: 0
        },
        announceConfirmDate: {
          name: 'announceConfirmDate',
          type: 'any'
        },
        announceBadgeConfirmDate: {
          name: 'announceBadgeConfirmDate',
          type: 'any'
        },
        realm: {
          name: 'realm',
          type: 'string'
        },
        username: {
          name: 'username',
          type: 'string'
        },
        credentials: {
          name: 'credentials',
          type: 'any'
        },
        challenges: {
          name: 'challenges',
          type: 'any'
        },
        verificationToken: {
          name: 'verificationToken',
          type: 'string'
        },
        status: {
          name: 'status',
          type: 'string'
        },
        created: {
          name: 'created',
          type: 'Date'
        },
        lastUpdated: {
          name: 'lastUpdated',
          type: 'Date'
        },
        id: {
          name: 'id',
          type: 'any'
        },
        createdAt: {
          name: 'createdAt',
          type: 'Date',
          default: new Date(0)
        },
        createdBy: {
          name: 'createdBy',
          type: 'string'
        },
        modifiedAt: {
          name: 'modifiedAt',
          type: 'Date',
          default: new Date(0)
        },
        modifiedBy: {
          name: 'modifiedBy',
          type: 'string'
        },
      },
      relations: {
        accessTokens: {
          name: 'accessTokens',
          type: 'any[]',
          model: ''
        },
      }
    }
  }
}
