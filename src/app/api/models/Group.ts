/* tslint:disable */

declare var Object: any;
export interface GroupInterface {
  name?: string;
  designId?: string;
  pages?: Array<any>;
  members?: Array<any>;
  url?: string;
  password?: string;
  statusDeleted?: string;
  id?: any;
  createdAt?: Date;
  createdBy?: string;
  modifiedAt?: Date;
  modifiedBy?: string;
  pageList?: any[];
  memberList?: any[];
}

export class Group implements GroupInterface {
  name: string;
  designId: string;
  pages: Array<any>;
  members: Array<any>;
  url: string;
  password: string;
  statusDeleted: string;
  id: any;
  createdAt: Date;
  createdBy: string;
  modifiedAt: Date;
  modifiedBy: string;
  pageList: any[];
  memberList: any[];
  constructor(data?: GroupInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Group`.
   */
  public static getModelName() {
    return "Group";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Group for dynamic purposes.
  **/
  public static factory(data: GroupInterface): Group{
    return new Group(data);
  }  
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Group',
      plural: 'Groups',
      properties: {
        name: {
          name: 'name',
          type: 'string'
        },
        designId: {
          name: 'designId',
          type: 'string',
          default: 'temp0001-01'
        },
        pages: {
          name: 'pages',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        members: {
          name: 'members',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        url: {
          name: 'url',
          type: 'string'
        },
        password: {
          name: 'password',
          type: 'string'
        },
        statusDeleted: {
          name: 'statusDeleted',
          type: 'string',
          default: '0'
        },
        id: {
          name: 'id',
          type: 'any'
        },
        createdAt: {
          name: 'createdAt',
          type: 'Date',
          default: new Date(0)
        },
        createdBy: {
          name: 'createdBy',
          type: 'string'
        },
        modifiedAt: {
          name: 'modifiedAt',
          type: 'Date',
          default: new Date(0)
        },
        modifiedBy: {
          name: 'modifiedBy',
          type: 'string'
        },
      },
      relations: {
        pageList: {
          name: 'pageList',
          type: 'any[]',
          model: ''
        },
        memberList: {
          name: 'memberList',
          type: 'any[]',
          model: ''
        },
      }
    }
  }
}
