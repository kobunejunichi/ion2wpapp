import { Component, ViewChild } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { LoopBackConfig } from './api/index';

// import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { SettingsPage } from '../pages/settings/settings';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  // rootPage = TabsPage;
  rootPage = LoginPage;
  settingsPage = SettingsPage;

  @ViewChild("rootNav") nav: NavController;

  constructor(platform: Platform) {
    // LoopBackの設定
    LoopBackConfig.setBaseURL('http://52.37.27.116');
    LoopBackConfig.setApiVersion('v1');
    // LoopBackConfig.setAuthPrefix("authorization");

    platform.ready().then(() => {
      // ステータスバーをデフォルトに設定
      StatusBar.styleDefault();
      // スプラッシュスクリーンを消す
      Splashscreen.hide();
    });
  }
  open(page: Component) {
    this.nav.push(page);
  }
  logout() {
    this.nav.setRoot(LoginPage);

  }
}
